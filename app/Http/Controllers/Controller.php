<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Spatie\Dropbox\Client as DropboxClient;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function DropboxInit()
    {
        $authorizationToken = 'lPSUXxelc2MAAAAAAAAAAeU4-p5iT2uhPA52XScAs_CerPv7zOGDd8M6AZXLGqXZ';

        $client = new DropboxClient($authorizationToken);

        return $client;
    }


    public function UnixTime()
    {
        return Carbon::now()->timestamp;
    }

    public function DateNow()
    {
        return Carbon::now()->format('Y-m-d');
    }
}
