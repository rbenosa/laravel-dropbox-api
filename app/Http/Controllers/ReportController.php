<?php

namespace App\Http\Controllers;

use App\Exports\ProductStockExport;
use App\Exports\VendorTotalStockExport;
use App\Imports\ProductStockImport;
use App\Imports\ProductVendorImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    
    public function get_report_1()
    {
        
        $product_stock      = $this->array_sort_by_column($this->getProductStock(), 'sku');
        $product_vendor     = $this->array_sort_by_column($this->getAllVendorCSV(), 'sku');
        

        
        foreach ($product_stock as $prd_key =>  $prd_stock) {

            $match = array_search($prd_stock['sku'], array_column($product_vendor, 'sku'));

            if ($match !== false) {
                $product_stock[$prd_key]['vendor'] = $product_vendor[$match]['vendor'];
            }
             
        }

        $export     = new ProductStockExport($product_stock);

        $file_name  = $this->DateNow() . '-stocks-vendor.csv';

        $upload     = $this->DropboxUpload($export, $file_name);


        return ($upload) ? true : abort(500);
    }



    public function get_report_2()
    {

        $product_stock      = $this->array_sort_by_column($this->getProductStock(), 'sku');
        $product_vendor     = $this->array_sort_by_column($this->getAllVendorCSV(), 'sku');


        foreach ($product_stock as $prd_key =>  $prd_stock) {

            $match = array_search($prd_stock['sku'], array_column($product_vendor, 'sku'));

            if ($match !== false) {
                $product_stock[$prd_key]['vendor'] = $product_vendor[$match]['vendor'];
            }

            if (intval($prd_stock['quantity']) < 0 || $prd_stock['quantity'] == 0) {
                unset($product_stock[$prd_key]);
            }

            
        }

        $export     = new ProductStockExport($product_stock);

        $file_name  = $this->DateNow() . '-stocks-vendor-withstocks.csv';

        $upload     = $this->DropboxUpload($export, $file_name);


        return ($upload) ? true : abort(500);

    }



    public function get_report_3()
    {

        $product_stock      = $this->array_sort_by_column($this->getProductStock(), 'sku');
        $product_vendor     = $this->array_sort_by_column($this->getAllVendorCSV(), 'sku');


        foreach ($product_stock as $prd_key =>  $prd_stock) {

            $match = array_search($prd_stock['sku'], array_column($product_vendor, 'sku'));

            if ($match !== false) {
                $product_stock[$prd_key]['vendor'] = $product_vendor[$match]['vendor'];
            }

            if (intval($prd_stock['quantity']) != 0 ) {
                unset($product_stock[$prd_key]);
            }
        }

        $export     = new ProductStockExport($product_stock);

        $file_name  = $this->DateNow() . '-stocks-vendor-withoutstocks.csv';

        $upload     = $this->DropboxUpload($export, $file_name);


        return ($upload) ? true : abort(500);
    }



    public function get_report_4()
    {
        
        $product_stock      = $this->getProductStock();
        $product_vendor     = $this->getAllVendorCSV();
        $vendor_total_stock = [];


        foreach ($product_stock as $prd_key =>  $prd_stock) {

            $match = array_search($prd_stock['sku'], array_column($product_vendor, 'sku'));

            if ($match !== false) {
                $product_stock[$prd_key]['vendor'] = $product_vendor[$match]['vendor'];
            }
            
        }

        $sorted = $this->array_sort_by_column($product_stock, 'vendor');

        $groupBy = $this->group_by('vendor', $sorted); 

        foreach ($groupBy as $grp_key => $grp_value) {

            $stock_count = 0;

            foreach ($grp_value as $value) {
                $stock_count += $value['quantity']; 
            }

            array_push($vendor_total_stock,[
                'vendor'    => $grp_key,
                'total'     => $stock_count,
            ]);
            
        }


        $export     = new VendorTotalStockExport($vendor_total_stock);

        $file_name  = $this->DateNow() . '-vendor-totalstocks.csv';

        $upload     = $this->DropboxUpload($export, $file_name);


        return ($upload) ? true : abort(500);

    }


    public function DropboxUpload($export, $file_name)
    {
        $client     = $this->DropboxInit();


        Excel::store($export, $file_name, 'files');

        $export_file = storage_path('files/' . $file_name);

        $file = file_get_contents($export_file);

        $upload =  $client->upload("/Devtest/Reports/" . $file_name, $file);


        $this->storageUnlink($file_name);


        return $upload;
    
    }


    public function group_by($key, $data)
    {
        $result = array();

        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val[$key]][] = $val;
            } else {
                $result[""][] = $val;
            }
        }

        return $result;
    }



    /* 
    * this will sort the array
    */
    public function array_sort_by_column($array, $col)
    {
        $keys = array_column($array, $col);

        array_multisort($keys, SORT_ASC, $array);

        return $array;
    }



    /* 
    * this function will get the Product csv file from dropbox and store it to storage folder for later use
    */
    public function getProductStock()
    {
        $client = $this->DropboxInit();

        $product_stock      = "Devtest/Stocks/all-products-stock.csv";
        $product_stock_csv  = $client->getTemporaryLink($product_stock);
        $file_name          = $this->UnixTime()."-product-stock.csv";

        $file = file_get_contents($product_stock_csv);
        file_put_contents(storage_path('files/' . $file_name), $file);

        $product_stock_path = storage_path('files/' . $file_name);
        $product_stock_file = new ProductStockImport();
        $product_stock_xls  = Excel::toArray($product_stock_file, $product_stock_path);

        $product_stocks_arr = [];


        foreach ($product_stock_xls[0] as $key => $value) {

            // get the second index from array
            if ($key > 0) {
                // set to new array
                array_push($product_stocks_arr, [
                    'sku'       => $value[0],
                    'quantity'  => $value[1],
                ]);
            }
        }


        $this->storageUnlink($file_name);

        return ($product_stocks_arr) ? $product_stocks_arr : abort(500);

    }



    /* 
    * this function will get the Vendor csv file from dropbox and store it to storage folder for later use
    */
    public function getAllVendorCSV()
    {
        $client = $this->DropboxInit();

        $product_vendor         = "Devtest/Vendor/all-products-vendor.csv";
        $product_vendor_csv     = $client->getTemporaryLink($product_vendor);
        $file_name              = $this->UnixTime() . "-product-vendor.csv";

        $file = file_get_contents($product_vendor_csv);
        file_put_contents(storage_path('files/' . $file_name), $file);

        $product_vendor_path    = storage_path('files/' . $file_name);
        $product_vendor_file    = new ProductVendorImport();
        $product_vendor_xls     = Excel::toArray($product_vendor_file, $product_vendor_path);

        $product_vendor_arr = [];


        foreach ($product_vendor_xls[0] as $key => $value) {

            // get the second index from array
            if ($key > 0) {
                // set to new array
                array_push($product_vendor_arr, [
                    'sku'       => $value[0],
                    'vendor'    => $value[1],
                ]);
            }
        }

        $this->storageUnlink($file_name);

        return ($product_vendor_arr) ? $product_vendor_arr : abort(500);
    
    }



    public function storageUnlink($file_name)
    {
        unlink(storage_path('files/' . $file_name));
    }

    

}