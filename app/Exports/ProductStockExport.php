<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class ProductStockExport implements FromArray, WithHeadings, ShouldAutoSize, WithStrictNullComparison
{
    use Exportable;

    protected $stocks;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(array $stocks)
    {
        $this->stocks = $stocks;
    }

    public function array(): array
    {
        return $this->stocks;
    }

    public function headings(): array {
        return [
            'SKU',
            'stock_quantity',
            'vendor'
        ];
    }

}
