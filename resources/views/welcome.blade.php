<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SmartChannel Exam</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

</head>

<body class="bg-light">

    <main>

        <div class="py-5 text-center">
            <h2>SmartChannel Report</h2>
        </div>



        <div class="col-md-4 offset-4">

            <div class="alert alert-dismissible fade report-alert" role="alert">
                <span class="report-message"></span>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>

            <center>
                <button class="btn btn-primary" id="report-1">Generate Report 1</button>
                <button class="btn btn-primary" id="report-2">Generate Report 2</button>
                <button class="btn btn-primary" id="report-3">Generate Report 3</button>
                <button class="btn btn-primary" id="report-4">Generate Report 4</button>
            </center>
        </div>


    </main>


    <script>
        $(document).ready(function() {

            xhr = null;

            $('#report-1').click(function() {

                let rpt_1_btn = $(this)

                xhr = $.ajax({
                    url: "{{ route('report-1') }}",

                    beforeSend: function() {
                        $(rpt_1_btn).attr('disabled', 'disabled');
                        $(rpt_1_btn).empty();
                        $(rpt_1_btn).append('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');


                        if (xhr != null) {
                            xhr.abort();
                        }
                    },

                    success: function(data) {

                        $(rpt_1_btn).removeAttr('disabled');
                        $(rpt_1_btn).empty();
                        $(rpt_1_btn).text('Generate Report 1');

                        $('.report-alert').addClass('show alert-success');
                        $('.report-message').html('<strong>Report 1 generated!</strong> You can now check your Dropbox.');

                        setTimeout(function() {
                            $('.report-alert').removeClass('show');
                        }, 5000);

                    },

                    error: function() {

                        $('.report-alert').addClass('show alert-danger');
                        $('.report-message').html('<strong>Opps! something went wrong.</strong> Please try again.');

                        setTimeout(function() {
                            $('.report-alert').removeClass('show');
                        }, 5000);

                        $(rpt_1_btn).removeAttr('disabled');
                        $(rpt_1_btn).empty();
                        $(rpt_1_btn).text('Generate Report 1');


                    }

                })

            })



            $('#report-2').click(function() {

                let rpt_2_btn = $(this)

                xhr = $.ajax({
                    url: "{{ route('report-2') }}",

                    beforeSend: function() {
                        $(rpt_2_btn).attr('disabled', 'disabled');
                        $(rpt_2_btn).empty();
                        $(rpt_2_btn).append('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');


                        if (xhr != null) {
                            xhr.abort();
                        }
                    },

                    success: function(data) {

                        $(rpt_2_btn).removeAttr('disabled');
                        $(rpt_2_btn).empty();
                        $(rpt_2_btn).text('Generate Report 2');

                        $('.report-alert').addClass('show alert-success');
                        $('.report-message').html('<strong>Report 2 generated!</strong> You can now check your Dropbox.');

                        setTimeout(function() {
                            $('.report-alert').removeClass('show');
                        }, 5000);

                    },

                    error: function() {

                        $('.report-alert').addClass('show alert-danger');
                        $('.report-message').html('<strong>Opps! something went wrong.</strong> Please try again.');

                        setTimeout(function() {
                            $('.report-alert').removeClass('show');
                        }, 5000);

                        $(rpt_2_btn).removeAttr('disabled');
                        $(rpt_2_btn).empty();
                        $(rpt_2_btn).text('Generate Report 2');


                    }

                })

            })



            $('#report-3').click(function() {

                let rpt_3_btn = $(this)

                xhr = $.ajax({
                    url: "{{ route('report-3') }}",

                    beforeSend: function() {
                        $(rpt_3_btn).attr('disabled', 'disabled');
                        $(rpt_3_btn).empty();
                        $(rpt_3_btn).append('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');


                        if (xhr != null) {
                            xhr.abort();
                        }
                    },

                    success: function(data) {

                        $(rpt_3_btn).removeAttr('disabled');
                        $(rpt_3_btn).empty();
                        $(rpt_3_btn).text('Generate Report 3');

                        $('.report-alert').addClass('show alert-success');
                        $('.report-message').html('<strong>Report 3 generated!</strong> You can now check your Dropbox.');

                        setTimeout(function() {
                            $('.report-alert').removeClass('show');
                        }, 5000);

                    },

                    error: function() {

                        $('.report-alert').addClass('show alert-danger');
                        $('.report-message').html('<strong>Opps! something went wrong.</strong> Please try again.');

                        setTimeout(function() {
                            $('.report-alert').removeClass('show');
                        }, 5000);

                        $(rpt_3_btn).removeAttr('disabled');
                        $(rpt_3_btn).empty();
                        $(rpt_3_btn).text('Generate Report 3');


                    }

                })

            })





            $('#report-4').click(function() {

                let rpt_4_btn = $(this)

                xhr = $.ajax({
                    url: "{{ route('report-4') }}",

                    beforeSend: function() {
                        $(rpt_4_btn).attr('disabled', 'disabled');
                        $(rpt_4_btn).empty();
                        $(rpt_4_btn).append('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...');


                        if (xhr != null) {
                            xhr.abort();
                        }
                    },

                    success: function(data) {

                        $(rpt_4_btn).removeAttr('disabled');
                        $(rpt_4_btn).empty();
                        $(rpt_4_btn).text('Generate Report 4');

                        $('.report-alert').addClass('show alert-success');
                        $('.report-message').html('<strong>Report 4 generated!</strong> You can now check your Dropbox.');

                        setTimeout(function() {
                            $('.report-alert').removeClass('show');
                        }, 5000);

                    },

                    error: function() {

                        $('.report-alert').addClass('show alert-danger');
                        $('.report-message').html('<strong>Opps! something went wrong.</strong> Please try again.');

                        setTimeout(function() {
                            $('.report-alert').removeClass('show');
                        }, 5000);

                        $(rpt_4_btn).removeAttr('disabled');
                        $(rpt_4_btn).empty();
                        $(rpt_4_btn).text('Generate Report 4');


                    }

                })

            })



        })
    </script>
</body>

</html>