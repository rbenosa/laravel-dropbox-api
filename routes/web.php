<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/get-report-1', [ReportController::class, 'get_report_1'])->name('report-1');
Route::get('/get-report-2', [ReportController::class, 'get_report_2'])->name('report-2');
Route::get('/get-report-3', [ReportController::class, 'get_report_3'])->name('report-3');
Route::get('/get-report-4', [ReportController::class, 'get_report_4'])->name('report-4');